DROP DATABASE IF EXISTS BDD_SELL;
CREATE DATABASE BDD_SELL;
USE BDD_SELL;

CREATE TABLE Client(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50),
    phone VARCHAR(50),
    email VARCHAR(255),
    default_address VARCHAR(255),
    FOREIGN KEY (default_address) REFERENCES Address(address_id),
);

CREATE TABLE Address(
    address_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    street VARCHAR(255),
    city VARCHAR(255),
    zipcode INT NOT NULL,
    state VARCHAR(255),
    country VARCHAR(255)
);
CREATE TABLE `Order`(
    order_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    client_id INT NOT NULL,
    delivery_address INT,  
    billing_address INT,   
    payment_method ENUM('Credit Card','bank check on delivery','cash on delivery'),
    order_state ENUM('cart','validated','sent','delivered'),
    FOREIGN KEY (client_id) REFERENCES Client(id),
    FOREIGN KEY (delivery_address) REFERENCES Address(address_id),
    FOREIGN KEY (billing_address) REFERENCES Address(address_id)
);


CREATE TABLE Product(
    product_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    description TEXT NOT NULL, 
    image VARCHAR(255),
    price FLOAT,
    available BOOLEAN
);


CREATE TABLE Order_lines(
    order_id INT NOT NULL,
    product_id INT NOT NULL,
    qt FLOAT, 
    FOREIGN KEY (order_id) REFERENCES `Order`(order_id),
    FOREIGN KEY (product_id) REFERENCES Product(product_id)
);

CREATE TABLE Client_address(
    client_id INT NOT NULL,
    address_id INT NOT NULL,
    PRIMARY KEY (client_id, address_id),  -- Added primary key
    FOREIGN KEY (client_id) REFERENCES Client(id),
    FOREIGN KEY (address_id) REFERENCES Address(address_id)
);