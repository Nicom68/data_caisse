USE BDD_SELL;

-- 1/Créer un produit “Truc”, avec comme description “Lorem Ipsum” comme image , qui coute 200 €.

INSERT INTO Product (name, description, image, price)
VALUES ("Truc", "Lorem Ipsum", 'https://picsum.photos/200', 200);
SELECT * from Product;

-- 2/Créer un produit “Bidule”, avec comme description “Lorem merol” comme image “https://picsum.photos/100”, qui coute 500 €.

INSERT INTO Product (name, description, image, price)
VALUES ("Bidule", "Lorem merol", 'https://picsum.photos/200', 500);
SELECT * from Product;

-- 3/Récupérer la liste des produits et vérifier le prix de Truc et de Bidule.
-- SELECT * from Product;

-- 4/Récupérer Truc à partir de son id et vérifier sa description et son image.
-- SELECT name, description, image
-- from Product
-- WHERE product_id = 1;

-- 8/Créer le client “Bob” avec comme mail “bob@gmail.com”.
-- INSERT INTO Client(name, email)
-- VALUES ('Bob', "bob@gmail.com");
-- SELECT * FROM Client;

-- 5/Créer une nouvelle commande.
-- INSERT INTO `Order`(order_id,client_id)
-- VALUES (1, 1);
-- SELECT * from `Order`;

-- 6/Ajouter 3 Trucs à la commande.(id de la commande = 1,product_id=1)
-- INSERT INTO `Order_lines`(order_id,product_id,qt)
-- VALUES(1,1,3);

-- 7/Ajouter 1 Bidules à la commande.(order_id=1,product_id)
-- INSERT INTO `Order_lines`(order_id,product_id,qt)
-- VALUES(1,20,1);

-- 8/Créer le client “Bob” avec comme mail “bob@gmail.com”.
-- INSERT INTO Client(name, email)
-- VALUES ('Bob', "bob@gmail.com");
-- SELECT * FROM Client;

-- 9/Mettre à jour Bob pour ajouter son numéro de téléphone “0123456789”.
-- UPDATE Client
-- SET phone = '0123456789'
-- WHERE id = "1";

-- SELECT * FROM Client

-- 10/Essayer de passer la commande dans l’état suivant (vérifier qu’il y a une erreur).
-- UPDATE `Order`
-- SET order_state = 'cart'
-- WHERE order_id = 1;

-- SELECT * from `Order`

-- 11/Associer Bob à la commande

-- UPDATE `Order`
-- WHERE client_id = 1;

-- SELECT * FROM `Order`;

-- 12/Créer l’adresse “12 street St, 12345 Schenectady, New York, US”
-- INSERT INTO Address (street, city, zipcode, state, country)
-- VALUES ('12 street St', 'Schenectady', '12345', 'New York', 'US');
-- SELECT * FROM Address;

-- 13/Essayer d’associer cette adresse comme facturation à la commande.Associer cette adresse à Bob.
-- UPDATE `Order`
-- SET billing_address=1
-- WHERE order_id = 1;
-- SELECT * FROM `Order`;

-- 14/Associer cette adresse à Bob.
-- UPDATE Client
-- SET default_address = 1
-- WHERE id = 1;
-- SELECT * FROM Client;

-- 15/Associer cette adresse comme facturation à la commande.
-- UPDATE `Order`
-- SET billing_address=1
-- WHERE order_id = 1;
-- SELECT * FROM `Order`;

-- 16/Créer l’adresse “12 boulevard de Strasbourg, 31000, Toulouse, OC, France”
-- INSERT INTO Address (street, city, zipcode, state, country)
-- VALUES ('12 boulevard de Strasbourg', 'Toulouse', '31000', 'OC', 'France');
-- SELECT * FROM Address;

-- 17/Associer cette adresse à Bob.
-- UPDATE Client
-- SET default_address = 2
-- WHERE id = 1;
-- SELECT * FROM Client;

-- 18/Associer cette adresse comme adresse de livraison.
-- UPDATE `Order`
-- SET delivery_address = 2
-- WHERE client_id = 1;
-- SELECT * from `Order`;

-- 19/Essayer de passer la commande dans l’état suivant (vérifier qu’il y a une erreur).
-- UPDATE `Order`
-- SET order_state = 'validated'
-- WHERE order_id = 1;
-- SELECT * from `Order`;

-- 20/Associer le moyen de paiement “Card” à la commande.
-- UPDATE `Order`
-- SET payment_method = 'credit card'
-- WHERE order_id = 1;
-- SELECT * from `Order`;

-- 21/Passer la commande dans l’état suivant.
-- UPDATE `Order`
-- SET order_state = 'sent'
-- WHERE order_id = 1;

-- 22/Vérifier que la commande est dans l’état “sent”.
-- SELECT * from `Order`;


-- 23/Récupérer la liste des adresses de Bob.

-- INSERT INTO Client_address(client_id, address_id) VALUES(1,1);

-- SELECT *
-- FROM Address
-- JOIN Client_address ON Address.address_id = Client_address.address_id
-- JOIN Client ON Client_address.client_id = Client.id
-- WHERE name='Bob';












