from fastapi import FastAPI
import mysql.connector
from models import Product, Order, Client

mydb = mysql.connector.connect(
    host="localhost",
    user="nicolas",
    password="DbxsvFb56K52A7;",
    database="BDD_SELL"
)

# uvicorn main:app --reload

app = FastAPI(debug=True)

# 1. Créer une route pour récupérer la liste des produits en json.
@app.get("/product")
async def create_product() -> list:
    mycursor = mydb.cursor(dictionary=True)
    sql = "select * from Product;"
    mycursor.execute(sql)
    liste_de_produit = mycursor.fetchall()

    return liste_de_produit

# 2. Créer une route pour récupérer l'id d'un produit en json.
#@app.get("/product/product_id")
@app.get("/product/{product_id}")
def get_product_id(product_id: int):
    mycursor = mydb.cursor(dictionary=True)
    query = "SELECT * FROM Product WHERE product_id=%s"
    mycursor.execute(query, (product_id,))
    result = mycursor.fetchone()
    return result

# 3. Ajouter le même produit en json (POST)
@app.post("/product/{id}")
def create_product(id : int , product: Product):
    mycursor = mydb.cursor(dictionary=True)

    # check if product already exists
    query_check = "SELECT * FROM Product WHERE product_id = %s"
    mycursor.execute(query_check, (id,))
    existing_product = mycursor.fetchone()
    
    if existing_product:
        return {"message": "Product with ID already exists"}
    else:
        insert_query = """
            INSERT INTO Product (id, name, description, image, price, available)
            VALUES (%s, %s, %s, %s, %s, %s)
        """
        insert_values = (id, Product.name, Product.description, Product.image, Product.price, Product.available)
        mycursor.execute(insert_query, insert_values)
        mydb.commit()

        return {
            "id": id,
            "name": Product.name,
            "description": Product.description,
            "image": Product.image,
            "price": Product.price,
            "available": Product.available
        }






    
    








