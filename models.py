from pydantic import BaseModel


class Product(BaseModel):
    id: int | None = None
    name: str 
    description : str 
    image : str 
    price : float
    available : bool 
  


class Order(BaseModel):
    id: int | None = None
    client: int | None = None


class Client(BaseModel):
    id: int | None = None
    name: str

class Adress(BaseModel):
    address_id: int | None = None
    street: str | None = None
    city: str | None = None
    zipcode: int | None = None
    state: str | None = None
    country: str | None = None